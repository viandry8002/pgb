const base_url = 'https://serverwan.com/PBP/API/public/api/';
export const key = 'base64:9M5f7o/6SD4nD1RxPrdB4OI55kDcXRyScT7JFkiku1M=';

// auth
export const postRegister = base_url + `register`;
export const postLogin = base_url + `login`;
export const postLogout = base_url + `logout`;
export const getCardMember = base_url + `card_member`;
export const postUpdateProfile = base_url + `me/update_profile`;
export const postUpdatePassword = base_url + `me/update_password`;
export const postSendOtp = base_url + `forget`;
export const postCheckOtp = base_url + `checkOtp`;
export const postResetPassword = base_url + `reset_password`;
export const getConfiguration = base_url + `configuration`;

// main menu home
export const getMe = base_url + `me`;
export const getSliders = base_url + `sliders`;
export const getAbout = base_url + `about`;
export const getOrganization = base_url + `organization`;
export const getGalleries = base_url + `galleries`;
export const getBranch = base_url + `cabang`;
export const getNews = base_url + `news`;
export const getMagazine = base_url + `majalah`;
export const getEvent = base_url + `acara`;
export const getMember = base_url + `member`;
