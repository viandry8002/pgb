import {StyleSheet, Text, View, ToastAndroid} from 'react-native';
import React from 'react';

const ToastMsg = msg => {
  ToastAndroid.show(msg, ToastAndroid.SHORT);
};

export default ToastMsg;

const styles = StyleSheet.create({});
