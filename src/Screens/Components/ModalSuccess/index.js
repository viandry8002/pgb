import React from 'react';
import {Image, Modal, StyleSheet, Text, View} from 'react-native';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import BtnPrimary from '../../Components/BtnPrimary';

const ModalSuccess = ({title, desc, press, showModal}) => {
  return (
    <Modal animationType="slide" transparent={true} visible={showModal}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Image
            source={require('../../../Assets/Icon/success.png')}
            style={{width: 60, height: 60}}
          />
          <View style={{height: 26}} />
          <Text style={[Font.bold16]}>{title}</Text>
          <View style={{height: 8}} />
          <Text
            style={[
              Font.normal14,
              {color: Colors.placholder, textAlign: 'center'},
            ]}>
            {desc}
          </Text>
          <View style={{height: 16}} />
          <View style={{width: 250}}>
            <BtnPrimary title={'Lanjut'} pressed={press} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default ModalSuccess;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
