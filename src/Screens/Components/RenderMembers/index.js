import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import Font from '../../../Styles/Fonts';

const RenderMembers = ({item}) => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 16,
        marginBottom: 16,
        alignItems: 'center',
      }}>
      <Image
        source={
          item?.file == null
            ? require('../../../Assets/Icon/person.png')
            : {uri: item.file}
        }
        style={{
          width: 48,
          height: 48,
          borderRadius: 100,
          marginRight: 8,
        }}
      />
      <Text style={[Font.semiBold14]}>{item.name}</Text>
    </View>
  );
};

export default RenderMembers;

const styles = StyleSheet.create({});
