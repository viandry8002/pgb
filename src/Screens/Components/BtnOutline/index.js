import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';

const BtnOutline = ({title, leftIcon, pressed}) => {
  return (
    <TouchableOpacity
      style={{
        height: 48,
        borderWidth: 1,
        borderColor: Colors.primary1,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
      }}
      onPress={pressed}>
      {leftIcon}
      <Text style={[Font.semiBold14, {color: Colors.primary1, marginLeft: 10}]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default BtnOutline;

const styles = StyleSheet.create({});
