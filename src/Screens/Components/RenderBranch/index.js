import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';

const RenderBranch = ({item}) => {
  const rowBranch = (icon, title, val) => (
    <View>
      <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
        {icon}
        <Text
          style={[Font.semiBold12, {marginHorizontal: 4, color: Colors.gray1}]}>
          {title}
        </Text>
        <Text style={[Font.normal12, {flex: 1, color: Colors.gray2}]}>
          {val}
        </Text>
      </View>
      <View style={{height: 8}} />
    </View>
  );
  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 16,
        marginBottom: 8,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Colors.gray6,
        padding: 16,
      }}>
      <Text style={[Font.semiBold14]}>{item.title}</Text>
      <View style={{height: 8}} />
      {/* Alamat */}
      <View style={{flex: 1, flexDirection: 'row'}}>
        <FontAwesome5 name="map-marker-alt" size={20} color={Colors.primary1} />
        <View style={{flex: 1, marginLeft: 4}}>
          <Text style={[Font.semiBold12, {color: Colors.gray1}]}>Alamat</Text>
          <Text style={[Font.normal12, {color: Colors.gray2}]}>
            {item.address}
          </Text>
        </View>
      </View>
      <View style={{height: 8}} />
      {/* Email */}
      {rowBranch(
        <MaterialCommunityIcons
          name="email"
          size={20}
          color={Colors.primary1}
        />,
        'Email',
        item.email,
      )}
      {/* PIC */}
      {rowBranch(
        <Ionicons name="person" size={20} color={Colors.primary1} />,
        'PIC',
        item.pic,
      )}
      {/* Phone */}
      {rowBranch(
        <FontAwesome name="phone" size={20} color={Colors.primary1} />,
        'Telpon',
        item.telp,
      )}
    </View>
  );
};

export default RenderBranch;

const styles = StyleSheet.create({});
