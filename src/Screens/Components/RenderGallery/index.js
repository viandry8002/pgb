import React from 'react';
import {Image, StyleSheet, View, Dimensions, Text} from 'react-native';
import Lightbox from 'react-native-lightbox-v2';

const windowWidth = Dimensions.get('window').width;

const RenderGallery = ({item}) => {
  console.log('aaaag', item);
  return (
    <Lightbox
      renderContent={() => (
        <Image
          source={{uri: item.file}}
          style={[
            {
              width: windowWidth - 32,
              height: 350,
              marginBottom: 2,
              alignSelf: 'center',
              resizeMode: 'contain',
            },
          ]}
        />
      )}>
      <Image
        source={{uri: item.file}}
        style={[
          {width: windowWidth * 0.5 - 4, marginBottom: 2},
          item.length == 'long' ? {height: 126} : {height: 100},
        ]}
      />
    </Lightbox>
  );
};

export default RenderGallery;

const styles = StyleSheet.create({});
