import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Input} from '@rneui/themed';
import Font from '../../../Styles/Fonts';
import Colors from '../../../Styles/Colors';

const FormInput = ({
  title,
  placholder,
  icon,
  rightIcon,
  scureText,
  titleCustom,
  val,
  change,
}) => {
  return (
    <View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        {icon}
        {titleCustom ? (
          titleCustom
        ) : (
          <Text style={[Font.semiBold12, {marginLeft: 8}]}>{title}</Text>
        )}
      </View>
      <Input
        value={val}
        onChangeText={change}
        label={false}
        placeholder={placholder}
        style={[Font.normal12, {color: Colors.placholder}]}
        inputContainerStyle={{
          borderColor: Colors.gray6,
          marginHorizontal: 0,
        }}
        containerStyle={icon ? {paddingHorizontal: 0} : false}
        inputStyle={{paddingHorizontal: 0}}
        rightIcon={rightIcon}
        secureTextEntry={scureText}
      />
    </View>
  );
};

export default FormInput;

const styles = StyleSheet.create({});
