import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';

const BtnPrimary = ({title, pressed, leftIcon, bgColor, colorTint}) => {
  return (
    <TouchableOpacity
      style={{
        height: 48,
        backgroundColor: bgColor ? bgColor : Colors.primary1,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
      }}
      onPress={pressed}>
      {leftIcon}
      <Text
        style={[
          Font.semiBold14,
          {color: colorTint ? colorTint : Colors.white},
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default BtnPrimary;

const styles = StyleSheet.create({});
