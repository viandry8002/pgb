import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import Colors from '../../../Styles/Colors';

const SpinnerLoad = ({loads}) => {
  return (
    <Spinner
      visible={loads}
      textContent={'Loading...'}
      textStyle={{color: Colors.primary1, fontSize: 12}}
    />
  );
};

export default SpinnerLoad;

const styles = StyleSheet.create({});
