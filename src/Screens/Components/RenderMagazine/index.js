import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';

const RenderMagazine = ({item, navi}) => {
  return (
    <TouchableOpacity style={{flex: 1}} onPress={navi}>
      <View
        style={[
          {
            flex: 1,
            margin: 4,
            backgroundColor: Colors.white,
            borderRadius: 8,
            elevation: 2,
          },
          item.title.length > 35 ? {height: 267} : {height: 250},
        ]}>
        <Image
          source={{uri: item.cover}}
          style={[
            {
              width: '100%',
              height: 200,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            },
          ]}
        />
        <View style={{flex: 1, padding: 8}}>
          <Text style={[Font.semiBold14]}>{item.title}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default RenderMagazine;

const styles = StyleSheet.create({});
