import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import moment from 'moment';
import 'moment/locale/id';

const RenderEvent = ({item}) => {
  const rowBranch = (icon, title, val, location) => (
    <View>
      <View style={{flexDirection: 'row'}}>
        {icon}
        <View style={{flex: 1, marginLeft: 4}}>
          <Text style={[Font.semiBold12]}>{title}</Text>
          {location}
          <Text style={[Font.normal12, {color: Colors.placholder}]}>{val}</Text>
        </View>
      </View>
      <View style={{height: 8}} />
    </View>
  );
  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 8,
        borderRadius: 16,
        padding: 8,
        elevation: 2,
        flexDirection: 'row',
        backgroundColor: Colors.white,
        marginBottom: 8,
      }}>
      <Image
        source={{uri: item.file}}
        style={{width: 105, height: 151, borderRadius: 16}}
      />
      <View style={{flex: 1, marginLeft: 8}}>
        <Text style={[Font.semiBold14, {color: Colors.gray1}]}>
          {item.title}
        </Text>
        <View
          style={{
            marginVertical: 8,
            borderBottomColor: Colors.gray6,
            borderBottomWidth: 1,
          }}
        />
        {rowBranch(
          <FontAwesome5
            name="calendar-alt"
            size={20}
            color={Colors.primary1}
          />,
          'Tanggal',
          moment(item.date_start).format('dddd, DD MMMM YYYY'),
        )}
        <View style={{height: 8}} />
        {rowBranch(
          <FontAwesome5
            name="map-marker-alt"
            size={20}
            color={Colors.primary1}
          />,
          'Lokasi',
          item.address,
          <Text style={[Font.normal12, {color: Colors.placholder}]}>
            {item.location}
          </Text>,
        )}
      </View>
    </View>
  );
};

export default RenderEvent;

const styles = StyleSheet.create({});
