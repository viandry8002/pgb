import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import moment from 'moment';
import RenderHtml from 'react-native-render-html';

const windowWidth = Dimensions.get('window').width;

const RenderNews = ({item, navi}) => {
  return (
    <TouchableOpacity
      style={{
        flex: 1,
        marginHorizontal: 16,
        borderRadius: 16,
        marginBottom: 8,
        elevation: 2,
      }}
      onPress={navi}>
      <Image
        source={{uri: item.file}}
        style={{
          width: '100%',
          height: 145,
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
        }}
      />
      <View
        style={{
          flex: 1,
          padding: 8,
          backgroundColor: Colors.white,
          borderBottomLeftRadius: 16,
          borderBottomRightRadius: 16,
        }}>
        <Text style={[Font.semiBold14]}>{item.title}</Text>
        <View style={{height: 8}} />
        <RenderHtml
          contentWidth={windowWidth - 32}
          tagsStyles={{p: {color: Colors.gray2}, li: {color: Colors.gray2}}}
          source={{
            html: item.description,
          }}
          defaultTextProps={{numberOfLines: 2}}
          enableExperimentalGhostLinesPrevention={true}
          enableExperimentalMarginCollapsing={true}
        />
        <View style={{height: 8}} />
        <Text style={[Font.normal12]}>
          {moment(item.created_at).format('DD MMM YYYY')}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default RenderNews;

const styles = StyleSheet.create({});
