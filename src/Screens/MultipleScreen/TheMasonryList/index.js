import MasonryList from '@react-native-seoul/masonry-list';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View, PermissionsAndroid} from 'react-native';
import Colors from '../../../Styles/Colors';
import {key, getGalleries, getMagazine} from '../../../Variable/ApiClient';
import RenderGallery from '../../Components/RenderGallery';
import RenderMagazine from '../../Components/RenderMagazine';
import RNFetchBlob from 'rn-fetch-blob';
import ToastMsg from '../../Components/ToastMsg';

const dataGalery = [
  {
    id: 1,
    file: require('../../../Assets/Image/image6.png'),
    length: 'long',
  },
  {
    id: 2,
    file: require('../../../Assets/Image/image7.png'),
    length: 'short',
  },
  {
    id: 3,
    file: require('../../../Assets/Image/image8.png'),
    length: 'short',
  },
  {
    id: 4,
    file: require('../../../Assets/Image/image9.png'),
    length: 'short',
  },
  {
    id: 5,
    file: require('../../../Assets/Image/image7.png'),
    length: 'short',
  },
  {
    id: 6,
    file: require('../../../Assets/Image/image8.png'),
    length: 'short',
  },
];

const dataMajalah = [
  {
    id: 1,
    file: require('../../../Assets/Image/magazine1.png'),
    title: 'Warta Bangau Edisi Februari 2020',
    length: 'short',
  },
  {
    id: 2,
    file: require('../../../Assets/Image/magazine2.png'),
    title: 'Warta Bangau Edisi Januari 2020',
    length: 'short',
  },
  {
    id: 3,
    file: require('../../../Assets/Image/magazine3.png'),
    title: 'Warta Bangau Edisi November 2019',
    length: 'short',
  },
  {
    id: 4,
    file: require('../../../Assets/Image/magazine4.png'),
    title: 'Warta Bangau Edisi Agustus-Oktober 2019',
    length: 'long',
  },
  {
    id: 5,
    file: require('../../../Assets/Image/magazine3.png'),
    title: 'Warta Bangau Edisi November 2019',
    length: 'long',
  },
  {
    id: 6,
    file: require('../../../Assets/Image/magazine4.png'),
    title: 'Warta Bangau Edisi Agustus-Oktober 2019',
    length: 'short',
  },
];

const TheMansoryList = ({navigation, route}) => {
  const [data, setData] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  useEffect(() => {
    navigation.setOptions({
      title: route.params.data.title,
    });
    getData();
  }, []);

  const paramApi = dt => {
    switch (dt) {
      case 'Galeri':
        return getGalleries;
      case 'Majalah':
        return getMagazine;
    }
  };

  const getData = () => {
    axios
      .get(paramApi(route.params.data.title), {
        headers: {
          key: key,
        },
      })
      .then(res => {
        res.data.success
          ? (setData(res.data.data.data),
            res.data.data.next_page_url === null
              ? (sethasScrolled(false), setPass(true))
              : setNextLink(res.data.data.next_page_url))
          : false;
      })
      .catch(err => {
        console.log('error', err);
      });
  };

  const requestStoragePermission = async aparam => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        actualDownload(aparam);
      } else {
        // Alert.alert('Peringatan', 'Izinkan File dan media terlebih dahulu', [
        //   {
        //     text: 'ok',
        //     onPress: () => console.log('Tidak'),
        //   },
        // ]);
        console.log('External storage permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const actualDownload = linkCard => {
    const {dirs} = RNFetchBlob.fs;
    RNFetchBlob.config({
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: `Majalah.pdf`,
        path: `${dirs.DownloadDir}/Majalah.pdf`,
      },
    })
      .fetch('GET', linkCard, {})
      .then(res => {
        // console.log('The file saved to ', res.path());
        ToastMsg('Majalah berhasil di simpan');
      })
      .catch(e => {
        console.log(e);
      });
  };

  const whatRender = (typeMenu, item) => {
    switch (typeMenu) {
      case 'Galeri':
        return <RenderGallery item={item} />;
      case 'Majalah':
        return (
          <RenderMagazine
            item={item}
            navi={() => requestStoragePermission(item.file)}
          />
        );
    }
  };

  const whatData = typeMenu => {
    switch (typeMenu) {
      case 'Galeri':
        return dataGalery;
      case 'Majalah':
        return dataMajalah;
    }
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    axios
      .get(`${nextLink}`, {
        headers: {
          key: key,
        },
      })
      .then(res => {
        sethasScrolled(!hasScrolled),
          setData([...data, ...res.data.data.data]),
          setNextLink(res.data.data.next_page_url);

        res.data.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.white,
      }}>
      <MasonryList
        // data={whatData(route.params.data.title)}
        // renderItem={whatRender(route.params.data.title)}
        data={data}
        // renderItem={whatRender(route.params.data.title)}
        renderItem={({typeMenu = route.params.data.title, item}) =>
          whatRender(typeMenu, item)
        }
        numColumns={2}
        showsVerticalScrollIndicator={false}
        style={{padding: 4}}
        // ListFooterComponent={() => <View style={{height: 16}} />}
        ListFooterComponent={() =>
          hasScrolled === true ? (
            <View style={{alignItems: 'center'}}>
              <Text style={{color: colors.black}}>Load more ....</Text>
            </View>
          ) : (
            <View style={{height: 16}} />
          )
        }
        onScroll={() => (pass === false ? onScroll() : false)}
        onEndReached={() => (nextLink === null ? false : handleLoadMore())}
      />
    </View>
  );
};

export default TheMansoryList;

const styles = StyleSheet.create({});
