import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import Colors from '../../../Styles/Colors';
import {
  key,
  postUpdatePassword,
  postResetPassword,
} from '../../../Variable/ApiClient';
import AlertMsg from '../../Components/AlertMsg';
import BtnPrimary from '../../Components/BtnPrimary';
import FormInput from '../../Components/FormInput';
import ModalSuccess from '../../Components/ModalSuccess';

const BlankForm = ({navigation, route}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [form, setForm] = useState({
    password_old: '',
    password: '',
    password_confirmation: '',
  });
  const [tokenAuth, setTokenAuth] = useState('');

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return setTokenAuth(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    navigation.setOptions({
      title: route.params.data.title,
    });
    getToken();
  }, []);

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const navigateTo = () => {
    setModalVisible(!modalVisible);
    route.params.data.title === 'Password Baru'
      ? navigation.goBack()
      : navigation.navigate('Login');
  };

  const updatePassword = () => {
    axios
      .post(postUpdatePassword, form, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + tokenAuth,
        },
      })
      .then(res => {
        res.data.success ? setModalVisible(true) : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const resetPassword = () => {
    axios
      .post(postResetPassword, form, {
        headers: {
          key: key,
          _alfa: route.params.data.data._alfa,
          _beta: route.params.data.data._beta,
        },
      })
      .then(res => {
        res.data.success ? setModalVisible(true) : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View style={styles.wrap}>
      {route.params.data.title === 'Password Baru' ? (
        <View style={{flex: 1}}>
          <FormInput
            title={'Kata Sandi Lama'}
            placholder={'Masukan sandi'}
            scureText={true}
            val={form.password_old}
            change={value => onInputChange(value, 'password_old')}
          />
          <FormInput
            title={'Kata Sandi Baru'}
            placholder={'Masukan sandi'}
            scureText={true}
            val={form.password}
            change={value => onInputChange(value, 'password')}
          />
          <FormInput
            title={'Konfirmasi Kata Sandi'}
            placholder={'Masukan ulang kata sandi'}
            scureText={true}
            val={form.password_confirmation}
            change={value => onInputChange(value, 'password_confirmation')}
          />
          <BtnPrimary title={'Simpan Data'} pressed={() => updatePassword()} />
        </View>
      ) : (
        <View style={{flex: 1}}>
          <FormInput
            title={'Kata Sandi'}
            placholder={'Masukan sandi'}
            scureText={true}
            val={form.password}
            change={value => onInputChange(value, 'password')}
          />
          <FormInput
            title={'Konfirmasi Kata Sandi'}
            placholder={'Masukan ulang kata sandi'}
            scureText={true}
            val={form.password_confirmation}
            change={value => onInputChange(value, 'password_confirmation')}
          />
          <BtnPrimary title={'Simpan Data'} pressed={() => resetPassword()} />
        </View>
      )}
      <ModalSuccess
        title={'Kata Sandi Berhasil Diubah'}
        desc={'Anda bisa login kembali menggunakan Kata Sandi baru anda'}
        press={() => navigateTo()}
        showModal={modalVisible}
      />
    </View>
  );
};

export default BlankForm;

const styles = StyleSheet.create({
  wrap: {flex: 1, backgroundColor: Colors.white, padding: 16},
});
