import React, {useEffect} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {List} from 'react-native-paper';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';

const AccordionList = ({navigation, route}) => {
  useEffect(() => {
    navigation.setOptions({
      title: route.params?.data.title,
    });
  }, []);

  const renderItem = ({item}) => {
    return (
      <View style={styles.containlist}>
        <List.Accordion
          title={item.title}
          titleStyle={[Font.semiBold14]}
          titleNumberOfLines={2}>
          <View style={styles.listAccor}>
            <Text style={[Font.normal14]}>{item.content}</Text>
          </View>
        </List.Accordion>
      </View>
    );
  };

  return (
    <View style={styles.wrap}>
      <FlatList
        data={route.params?.data.subs}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

export default AccordionList;

const styles = StyleSheet.create({
  wrap: {flex: 1, backgroundColor: Colors.white, padding: 16},
  containlist: {
    flex: 1,
    marginBottom: 16,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: Colors.gray6,
    backgroundColor: Colors.white,
  },
  listAccor: {marginHorizontal: 16, marginVertical: 8},
});
