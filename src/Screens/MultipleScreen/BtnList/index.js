import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {getAbout, getOrganization, key} from '../../../Variable/ApiClient';

const dataAboutUs = [
  {
    id: 1,
    title: 'Tentang Bangau Putih',
    navigateTo: 'Description',
    data: {
      title: 'Tentang Bangau Putih',
      image: '',
      desc: `Perguruan Silat Persatuan Gerak Badan (PGB) 
      Bangau Putih berdiri sejak tanggal 25 Desember 1952. Pendiri adalah (alm) Suhu Subur Rahardja. 
      Penerus sekarang adalah Guru Besar Gunawan Rahardja. Perguruan ini berpusat di Bogor, beralamat di Jl. Kebon Jukut No,1, Sukasari - Bogor 16142.`,
    },
  },
  {
    id: 2,
    title: 'Sejarah Perguruan',
    navigateTo: 'Description',
    data: {
      title: 'Masa Awal (1950-1952)',
      image: require('../../../Assets/Image/history1.png'),
      desc: `Persatuan Gerak Badan Bangau Putih lahir dari kegemaran Suhu Subur Rahardja terhadap ilmu beladiri. Beliau lahir dengan nama Liem Sin Tjoei pada tanggal 4 April 1925. Ibunya bernama Tan Kim Nio dan ayahnya bernama Liem Kim Sek. Darah silat dan pengobatan mengalir dari keluarganya.
      Pada saat Suhu Subur Rahardja berusia 6 tahun, Liem Kim Sek meninggal dunia. Subur Rahardja kemudian diangkat anak oleh pamannya yang bernama Liem Kim Bouw. Dengan asuhan Liem Kim Bouw, Subur Rahardja tumbuh berkembang menjadi seorang pendekar pada zamannya.
      Pada tahun 1952, Subur Rahardja beserta para sahabat dan keluarga yang bernama Tjio Wie Kwat, Liem Sin Tjoan, Kwee Hian Tjie, dan Liem Kie Gwan berlatih serta bertukar ilmu. Oleh karena kepiawaian mereka dalam bersilat dan jumlah mereka yang 5 (lima) orang maka masyarakat saat itu mulai menjuluki mereka sebagai Lima Macan Pulo.`,
    },
  },
  {
    id: 3,
    title: 'Mukadimah Guru Besar',
    navigateTo: 'Description',
    data: {
      title: '',
      image: require('../../../Assets/Image/history2.png'),
      desc: `Mukadimah ini dibacakan pertama kali pada tanggal 27 September 1977, pada malam perayaan Malam Purnama dan dinyatakan sebagai Mukadimah Guru Besar untuk seluruh Perguruan dimanapun Perguruan tersebut berada. \n\n (Copyright © 2015 PGB Bangau Putih)`,
    },
  },
  {
    id: 4,
    title: 'Keilmuan PGB Bangau Putih',
    navigateTo: 'BtnList',
    data: [
      {
        title: 'Silat',
        image: require('../../../Assets/Image/image4.png'),
        desc: `Silat memberikan berbagai macam pelatihan 
        dan peluang pengembangan kepribadian diri
        yang baik untuk semua kalangan. Silat juga memberikan gerakan-gerakan untuk kesehatan.
        Dasar–dasar utama dari Silat PGB Bangau Putih 
        adalah Memelihara Kepribadian, Patuh pada 
        Kejujuran, Menjaga Sopan Santun 
        (Keharmonisan), Mempertinggi prestasi diri, dan Kemampuan untuk Menguasai diri.
        Silat di dalam PGB Bangau Putih mempunyai gerakan yang selaras dan harmonis, gerakan-gerakan tersebut merupakan karakteristik dari Silat itu sendiri.
        Bentuk dari gerakan-gerakan silat PGB Bangau Putih memiliki kelembutan, bentuk tangkisan, serta teknik pukulan-pukulan yang meyakinkan. 
        Dengan menguasai gerakan-gerakan silat tesebut tidak hanya berguna untuk bela diri atau pertahanan tubuh, tetapi juga berguna dalam kehidupan sehari-hari (seperti membangun mental dan menguasi emosi).`,
        navigateTo: 'Description',
      },
      {
        title: 'Tao Kung',
        image: require('../../../Assets/Image/history3.png'),
        desc: `Tao Kung berarti gerak untuk keharmonisan dan keselarasan. 
        Taokung dikembangkan dari gerakan silat PGB Bangau Putih dan merupakan seni gerak badan untuk kesehatan. 
        Gerak Tao Kung mengolah tenaga dan mengendapkan hawa yang bekerja simultan pada sistem pernapasan untuk keseimbangan kerja otak serta meridian. 
        Tao Kung yang terdiri dari beberapa paket, dan masing-masing memiliki 9 gerak dan 3 pernapasan yang bersifat preventif. 
        Banyak kasus kesehatan yang teratasi berkat latihan secara terus menerus, dan niat positif dari diri yang bersangkutan`,
        navigateTo: 'Description',
      },
      {
        title: 'Sam Po Kun',
        image: require('../../../Assets/Image/image5.png'),
        desc: `Mind, Body, Spirit merupakan bagian internal dalam diri manusia. 
        Hanya dengan menyatukan ketiganya seseorang diandaikan bisa melaksanakan aktivitas dengan baik, 
        bisa menyelenggarakan hidup dengan harmonis. Ada keseimbangan antara mind, body, spirit. 
        Aktivitas mind, body, spirit secara maksimal dapat dilatih, latihan dilakukan lewat olah tubuh, 
        guna membangunkan kesadaran atas semua yang terkait dan terkandung di dalamnya, dari sistem syaraf, urat, otot, organ, sampai pada pikiran (mind), dan semangat (spirit). 
        Dengan integrasi ketiganya, yang berarti adanya keseimbangan dari diri kita, maka terselenggara pula harmoni antara diri kita dengan alam, dimana kita hidup. 
        Kita bisa menyelanggarakan kehidupan dalam kewajaran, baik di lingkungan terkecil seperti keluarga, sampai pada lingkungan profesi dan lainnya atau lebih luas lagi lingkungan masyarakat.
        PGB Bangau Putih membagikan pengenalan mind, body,spirit lewat pelatihan MBS. Pelatihan ini terbuka bagi semua kalangan, terutama bagi yang berusia 40 tahun ke atas. Instruktur terdiri dari para pelatih PGB Bangau Putih.
        `,
        navigateTo: 'Description',
      },
    ],
  },
  {
    id: 5,
    title: 'Etika Perguruan',
    navigateTo: 'AccordionList',
    data: [
      {
        id: 1,
        pertanyaan: 'Apa Artinya Lingkaran Pembukaan?',
        jawaban: `Sikap awal dan akhir melambangkan posisi/kondisi siap mental dan fisik dalam menjalani latihan.
          Tangan kanan terbuka di depan dada kiri, artinya mengikuti latihan dengan tulus dan percaya menerima bimbingan dan arahan dari pelatih.
          Tangan kiri tegak lurus mengarah ke atas, artinya dengan jujur dan tulus hati menjunjung tinggi nilai-nilai persaudaraan.
          Sikap membungkuk artinya di lima benua dan empat penjuru dunia, kita adalah saudara atas dasar saling menghormati.
          Pembukaan jalan panjang diartikan sebagai etika perguruan dan adat sopan santun memasuki ruangan khusus. Dengan melakukan pembukaan dan penutupan, menunjukan kekhasan jalan panjang agar dilakukan secara fokus, konsentrasi, meditatif, tidak mengijinkan diri kita diganggu hal-hal lain. Karena ruangan itu adalah jiwa kita.`,
        urutan: '1',
        tampilkan: 'YES',
        tgl: '2019-12-09 14:12:34',
        create_by: '15',
      },
      {
        id: 2,
        pertanyaan: 'Lingkaran Pembukaan Kenapa Mundur 3 Langkah?',
        jawaban: `Sikap awal dan akhir melambangkan posisi/kondisi siap mental dan fisik dalam menjalani latihan.
        Tangan kanan terbuka di depan dada kiri, artinya mengikuti latihan dengan tulus dan percaya menerima bimbingan dan arahan dari pelatih.
        Tangan kiri tegak lurus mengarah ke atas, artinya dengan jujur dan tulus hati menjunjung tinggi nilai-nilai persaudaraan.
        Sikap membungkuk artinya di lima benua dan empat penjuru dunia, kita adalah saudara atas dasar saling menghormati.
        Pembukaan jalan panjang diartikan sebagai etika perguruan dan adat sopan santun memasuki ruangan khusus. Dengan melakukan pembukaan dan penutupan, menunjukan kekhasan jalan panjang agar dilakukan secara fokus, konsentrasi, meditatif, tidak mengijinkan diri kita diganggu hal-hal lain. Karena ruangan itu adalah jiwa kita.`,
        urutan: '2',
        tampilkan: 'YES',
        tgl: '2019-12-09 14:14:49',
        create_by: '15',
      },
      {
        id: 3,
        pertanyaan: 'Mengapa Lambang Ada di Dada Kiri?',
        jawaban: `Sikap awal dan akhir melambangkan posisi/kondisi siap mental dan fisik dalam menjalani latihan.
        Tangan kanan terbuka di depan dada kiri, artinya mengikuti latihan dengan tulus dan percaya menerima bimbingan dan arahan dari pelatih.
        Tangan kiri tegak lurus mengarah ke atas, artinya dengan jujur dan tulus hati menjunjung tinggi nilai-nilai persaudaraan.
        Sikap membungkuk artinya di lima benua dan empat penjuru dunia, kita adalah saudara atas dasar saling menghormati.
        Pembukaan jalan panjang diartikan sebagai etika perguruan dan adat sopan santun memasuki ruangan khusus. Dengan melakukan pembukaan dan penutupan, menunjukan kekhasan jalan panjang agar dilakukan secara fokus, konsentrasi, meditatif, tidak mengijinkan diri kita diganggu hal-hal lain. Karena ruangan itu adalah jiwa kita.`,
        urutan: '1',
        tampilkan: 'YES',
        tgl: '2019-12-09 14:12:34',
        create_by: '15',
      },
      {
        id: 4,
        pertanyaan: 'Mengapa Sabuk Laki-Laki Di Kiri dan Perempuan Di Kanan?',
        jawaban: `Sikap awal dan akhir melambangkan posisi/kondisi siap mental dan fisik dalam menjalani latihan.
        Tangan kanan terbuka di depan dada kiri, artinya mengikuti latihan dengan tulus dan percaya menerima bimbingan dan arahan dari pelatih.
        Tangan kiri tegak lurus mengarah ke atas, artinya dengan jujur dan tulus hati menjunjung tinggi nilai-nilai persaudaraan.
        Sikap membungkuk artinya di lima benua dan empat penjuru dunia, kita adalah saudara atas dasar saling menghormati.
        Pembukaan jalan panjang diartikan sebagai etika perguruan dan adat sopan santun memasuki ruangan khusus. Dengan melakukan pembukaan dan penutupan, menunjukan kekhasan jalan panjang agar dilakukan secara fokus, konsentrasi, meditatif, tidak mengijinkan diri kita diganggu hal-hal lain. Karena ruangan itu adalah jiwa kita.`,
        urutan: '2',
        tampilkan: 'YES',
        tgl: '2019-12-09 14:14:49',
        create_by: '15',
      },
    ],
  },
  {
    id: 6,
    title: 'Logo Perguruan',
    navigateTo: 'Description',
    data: {
      title: '',
      image: require('../../../Assets/Image/logo2.png'),
      desc: `Apa Artinya Lambang PGB? \n\n
      Warna kuning pada lingkaran luar melambangkan persaudaraan. Artinya, persaudaraan kita junjung tinggi di manapun kita berada.
      Warna kuning pada lingkaran dalam adalah simbol keilmuan. Artinya, ilmu silat yang kita miliki bukan untuk menjadikan kita sombong. Tapi berada di dalam diri sebagai dasar pembentukan kepribadian dan kepercayaan diri.
      Warna biru sebagai simbol keharmonisan. Silat sebagai ilmu yang memperkuat tenaga hubungan harmonis antar manusia, bukan untuk kekerasan dan memecah belah.
      Warna merah melambangkan keberanian. Sifat manusia pesilat yang berani membela kebenaran.
      Warna putih melambangkan kemurnian. Persaudaraan dan keilmuan dilandaskan cinta kasih.
      Dua lingkaran kecil di kanan kiri melambangkan keseimbangan.
      Burung bangau senang hidup berkumpul, tidak pernah mencari musuh dan selalu ingat sarangnya. Burung bangau melambangkan persaudaraan dan sifat-sifat terpuji.
      Paruh bangau yang keluar dari lingkaran artinya perguruan selalu membuka pintu kepada siapa saja yang bersimpati pada perguruan walau bukan siswa PGB.
      Kaki bangau yang keluar dari lingkaran artinya setiap siswa PGB yang sudah non aktif dan ingin bergabung kembali akan diterima jika bersedia mengikuti ketentuan perguruan.`,
    },
  },
  {
    id: 7,
    title: 'Tradisi Perguruan',
    navigateTo: 'Description',
    data: {
      title: '',
      image: '',
      desc: `Adapun jalan hidup perguruan ini adalah mengutamakan rasa kemanusiaan, kasih sayang, dan persaudaraan untuk mencapai mental 
      dan kepribadian yang luhur serta keseimbangan jiwa dalam melakukan kewajaran sebagai manusia seutuhnya yang
       selalu berusaha mendekatkan diri dengan Sang Pencipta, alam semesta, dan seluruh isinya.
      Perguruan Silat Persatuan Gerak Badan (PGB) Bangau Putih setiap tahun menyelenggarakan beberapa acara tradisi perguruan, antara lain;
      \n
      1. Perayaan Imlek
      \n
      Perayaan tahun baru Imlek diadakan setiap malam pergantian tahun baru Imlek. Perayaan ini selalu diadakan di rumah perguruan serta menjadi suatu tradisi yang tidak dapat ditinggalkan. 
      Perayaan ini sebagai wujud syukur pada Tuhan YME atas berkah dan karunia yang telah diberikan pada kelangsungan perguruan dan anggota keluarga besarnya, juga sebagai silaturahmi dalam mempererat tali persaudaraan dan kebersamaan diantara anggota keluarga besarnya.
      Semenjak tahun 2010, anggota rumah perguruan membuat kegiatan dengan sambil menunggu waktu upacara ritual perguruan, yaitu dengan mengadakan acara “Imlek Tempo Doeloe,” dengan menampilkan hiburan berupa Orkes Keroncong “Asmara”, binaan Perguruan Silat PGB Bangau Putih.
      \n
      2. Peh Gwee Cap Go
      \n
      Pertama kali perayaan malam Bulan Purnama di perguruan kita berlangsung di rumah Gg. Angbun No. 79, Bogor, tahun 1953. 
      Pada masa itu, menyambut acara ini setiap murid membawa satu batu bulat untuk diletakkan di meja altar. 
      Batu yang disusun melingkar pada sebuah wadah itu membawa arti bahwa dalam setiap diri anggota terdapat rasa persaudaraan yang erat. 
      Sedangkan batu bulat sebesar kelereng membawa arti dalam setiap diri anggota mempunyai kebulatan tekad untuk berlatih serius di PGB.
      Acara ini adalah kegiatan intern perguruan yang diadakan setiap tanggal 15 (cap go) bulan ke delapan (peh gwee) menurut penanggalan Tionghoa. 
      Pada bulan tersebut, (purnama) rembulan lebih besar dibandingkan tanggal 15 pada bulan-bulan lainnya.
      Perayaan malam bulan purnama sejak tahun 1970 sampai sekarang, diadakan di Pusat Perguruan, Jl. Kebon Jukut No.1, Bogor. 
      Persiapan menjelang perayaan tersebut sudah tampak sejak dua minggu sebelumnya. Dimulai dengan pengecatan tembok ruang latihan, rumah, pintu bangau, sampai ke dapur dan kamar mandi/kecil. Diikuti dengan membuat dekorasi ruangan latihan, penataan lilin, sampai penyajian bubur nasi spesial Peh Gwee Cap Go. 
      Semua persiapan dikerjakan oleh anggota padepokan perguruan.
      `,
    },
  },
];

const dataOrganization = [
  {
    id: 1,
    title: 'Struktur Perguruan',
    navigateTo: 'Description',
    data: {
      title: '',
      image: '',
      desc: '',
    },
  },
  {
    id: 2,
    title: 'Agenda 2019',
    navigateTo: 'Description',
    data: {
      title: '',
      image: '',
      desc: '',
    },
  },
  {
    id: 3,
    title: 'Prosedur Kunjungan',
    navigateTo: 'Description',
    data: {
      title: 'Kunjungan Murid ke Pusat Perguruan',
      image: '',
      desc: `Untuk anggota PGB Bangau Putih dari wilayah Indonesia, yang akan mengunjungi Pusat Perguruan, untuk tinggal dan berlatih, diwajibkan mengikuti peraturan yang tercantum dibawah ini, yaitu:
      Menulis surat permohonan kepada Dewan Keorganisasian yang telah diketahui dan ditandatangi oleh orang tua, pengurus cabang atau grup, serta pelatih dan mengirimkan melalui email atau pos (cantumkan nomor kontak yang bisa dihubungi.)
      Tunggu jawaban dari Pusat, dan jawaban akan dikirim melalui email atau telepon langsung.
      Jika diperkenankan, silahkan langsung datang ke Pusat Perguruan.
      \n
      Keterangan lain:
      \n
      Jika anggota datang berkunjung ke PGB Bangau Putih Pusat Bogor, hanya untuk berlatih, diharuskan membawa surat keterangan dari pengurus cabang, grup dan disetujui oleh pelatih.
      Jika pengurus cabang beserta anggotanya akan berkunjung ke PGB Bangau Putih Pusat Bogor, silahkan mengirim surat permohonan kunjungan beserta maksud dan tujuannya ke Organisasi Pusat.
      `,
    },
  },
  {
    id: 4,
    title: 'Peringkat Sabuk Silat',
    navigateTo: 'Description',
    data: {
      title: 'Tanda Pengenal Peringkat Anggota, Update Agustus 2016',
      image: require('../../../Assets/Image/history4.png'),
      desc: '',
    },
  },
  {
    id: 5,
    title: 'Activity',
    navigateTo: 'BtnList',
    data: [
      {
        title: 'Program Kakilangit',
        image: require('../../../Assets/Image/history5.png'),
        desc: `LATAR BELAKANG Dalam menyikapi Mukadimah Guru Besar PGB Bangau Putih, kami Dewan Keorganisasian PGB Bangau Putih Pusat-Bogor mewujudkannya di lingkungan masyarakat sekitar Padepokan PGB Bangau Putih di Desa Sampay, Kelurahan Tugu Selatan, Kec. Cisarua, Kab. Bogor, dengan program yang diberi nama Kaki Langit. Program ini merupakan salah satu bagian dari kepedulian PGB Bangau Putih terhadap lingkungan sekitar, khususnya dalam hal pengembangan sumber daya manusia bagi generasi penerus terutama anak-anak dan remaja.
        Perguruan Silat PGB Bangau Putih dalam dinamika keberadaannya, kini di usia yang akan mencapai ke-64 tahun, sudah selayaknya jika Dewan Keorganisasian PGB Bangau Putih melengkapi pirantinya untuk merespon perkembangan zaman. Hingga, pada waktunya, dapat menjadi sebuah lembaga yang kehadirannya mampu memberikan sumbangsih pada pengembangan arah pendidikan dan kebudayaan nasional.
        Sarana padepokan perguruan yang mencukupi akan dijadikan sebagai sentra “Laboratorium Silat PGB Bangau Putih”, yang kemudian disingkat menjadi Silatorium, dapat meliputi orang dan juga tempat.
        Sebagai tempat, secara terbuka atupun tertutup; yang dipergunakan untuk melakukan kegiatan pendidikan maupun pelatihan ilmu silat yang berhubungan atau berkaitan dengan disiplin ilmu lain.
        Sedangkan sebagai orang adalah pelaku kegiatan tersebut; penggiat seni olah-gerak dapat menerapkan pelatihan yang berkaitan dengan disiplin ilmu lain.
        `,
        navigateTo: 'Description',
      },
      {
        title: 'PGB Peduli',
        image: require('../../../Assets/Image/history6.png'),
        desc: `PGB Peduli digagas oleh Ibu Hettyana Yasin dan Ibu Megawati Lie pada akhir tahun 2012. Seringnya mereka menyelenggarakan bakti sosial di luar Bogor menimbulkan ide untuk mengadakan kegiatan serupa di daerah Bogor dan sekitarnya.
        Kegiatan bakti sosial merupakan bentuk kepedulian dan tanggung jawab sosial PGB Bangau Putih untuk dapat memberikan manfaat terhadap masyarakat. Kegiatan bakti sosial terselenggara atas kerjasama dengan Yayasan Dhanagun Bogor dan We Share Community. Dimana bakti sosial ini merupakan suatu bentuk bantuan yang diharapkan dapat berguna untuk membantu meringankan masyarakat sekitar dalam hal kesehatan. Kegiatan ini juga menjelma menjadi sebuah jembatan antara kepedulian kami sebagai organisasi sosial, terhadap mereka yang membutuhkan bantuan kesehatan, khususnya bagi janda dan anak yatim piatu yang tidak mampu secara ekonomi.
        Kini, perguruan silat Persatuan Gerak Badan (PGB) Bangau Putih, memiliki wadah untuk berbagi dengan sesama sekaligus dapat menjadi perpanjangan tangan dari orang-orang yang ingin membantu. Keanggotaan PGB Peduli terbuka untuk seluruh keluarga besar perguruan dengan sumber dana diperoleh dari para donatur.`,
        navigateTo: 'Description',
      },
      {
        title: 'Kie Lin PGB',
        image: require('../../../Assets/Image/history7.png'),
        desc: `Beberapa kebudayaan unik di Bogor yang berhasil dipertahankan saat ini hanyalah Kie Lin. Itu pun dengan berbagai kerja keras yang harus ditempuh oleh para sesepuh Persatuan Gerak Badan (PGB) Bangau Putih. Perguruan silat ini berkali-kali memindahkan Kie Lin agar bisa bertahan. Bahkan, Kie Lin di Bogor setidaknya sudah tiga kali dibuat, awal tahun 1950, 1954 yang saat itu dibuat dari bahan terpal, dan yang terbaru adalah Kie Lin yang saat ini ada dan bertahan di Kebon Jukut. “Awalnya kita simpan di Gedong Dalam, terus pernah dua tahun di Pulasara, sampai akhirnya kini di Kebon Jukut,” cerita Gunawan Rahardja, pimpinan PGB Bangau Putih yang merupakan putra Subur Rahardja atau Lim Sin Tjoei. Subur pada jamannya dikenal sebagai inohong yang sangat berwibawa dan begitu lekat dengan kehidupan sosial dan masyarakat di Bogor.
        Satu-satunya Kie Lin di seluruh Indonesia, hanya ada di Bogor, dihidupkan dan dilestarikan oleh anggota Persatuan Gerak Badan Bangau Putih. Tarian dan musik Kie Lin dapat dikatakan sangat langka dan khas serta tidak ada perguruan silat atau perkumpulan kesenian barong yang memiliki Kie Lin sebagai barongnya.
        `,
        navigateTo: 'Description',
      },
    ],
  },
  {
    id: 6,
    title: 'Member',
    navigateTo: 'Description',
    data: {
      title: '',
      image: '',
      desc: '',
    },
  },
  {
    id: 7,
    title: 'Pengurus Organisasi',
    navigateTo: 'Description',
    data: {
      title: '',
      image: require('../../../Assets/Image/history8.png'),
      desc: `
      Pengurus Dewan Keorganisasian periode tahun 2016-2020 \n
      Ketua Umum, Widya Poerwoko
      Ketua I Bidang Operasional, Rahadianto
      Ketua II Bidang Keuangan, Jusdi Fadjar
      Sekretaris I, Anggono Wisnudjati
      Sekretaris II, Subiyanto
      Bendahara I, Raditya Harsa
      Bendahara II,Vieta
      \n
      HUBUNGAN MASYARAKAT \n
      Jemmy Charter
      Nurdin Alamsyah
      Widya Puspita
      Heryanto La Terza
      \n
      PENDANAAN \n
      Joseph Nazary
      Jusdi Fadjar
      \n
      SOSIAL \n
      Hettyana Yasin Rahardja
      Yanti Tjiu
      Erna Tjiu
      Ferry Wong
      Peter Evert
      Suyeni Amin
      \n
      PENELITIAN dan PENGEMBANGAN \n
      Stefhanie P.Rahardja
      Jacobus Susanto
      Yan Fernando
      Henky
      Anthonius G.Rahardja
      Andy Liao
      \n
      `,
    },
  },
  {
    id: 8,
    title: 'Program Perguruan',
    navigateTo: 'Description',
    data: {
      title: 'Program Dewan Keorganisasian',
      image: '',
      desc: `
        Program Kerja Dewan Keorganisasian Tahun 2016 - 2020
        \n
        Visi\n
        Mengembalikan Ilmu Kepada Alam Melalui Masyarakat dan Kebudayaan
        Melestarikan Kebudayaan Silat PGB Bangau Putih
        Mendirikam Lembaga Pendidikan dan Pelatihan Silat
        \n
        Misi \n
        Memperbaiki sistem dan basis data Pusat, Cabang, Magang dan Grup latihan di seluruh Wilayah Perguruan Silat PGB Bangau Putih
        Memetakan potensi yang dimiliki oleh Perguruan dari berbagai aspeknya baik itu yang bersifat keilmuan maupun pengorganisasian
        Melakukan perbaikan pelaksanaan kegiatan upacara/ritual Perguruan
        \n
        IDE DASAR \n
        SILATSCHOOL Institut Silat. Menata dan meningkatkan kemampuan manajemen pengelolaan Dewan Keorganisasian Perguruan Silat PGB Bangau Putih, baik itu ditingkat pusat maupun daerah
        SILATORIUM Laboratorium Silat. Menyatupadukan langkah dan arah antara Dewan Keorganisasian dengan Dewan Keilmuan PGB Bangau Putih untuk mencapai tujuan yang telah disepakati bersama.
        SILATOUR Tour Jaringan Silat. Menjalin dan menyelaraskan kerjasama antara Dewan Keorganisasian, Dewan Keilmuan dan Dewan Usaha untuk mengembangkan jaringan pendidikan silat di seluruh wilayah tanah air Indonesia
        4. SILATPUSTAKA Perpustakaan. Mengembangkan Buletin Warta Bangau dan Majalah D’EcoArt sebagai media interaksi keilmuan lintas disiplin, dan menjadikan keduanya sebagai bahan informasi serta kajian ilmu silat dan penerapannya.
        `,
    },
  },
];

const BtnList = ({navigation, route}) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    navigation.setOptions({
      title: route.params.data.title,
    });
    route.params.data.title === 'Keilmuan PGB Bangau Putih' ||
    route.params.data.title === 'Etika Perguruan' ||
    route.params.data.title === 'Activity'
      ? setData(route.params.data.subs)
      : getData();
  }, []);

  const paramApi = dt => {
    switch (dt) {
      case 'Tentang Kami':
        return getAbout;
      case 'Organisasi':
        return getOrganization;
      case 'Keilmuan PGB Bangau Putih':
        return route.params.data.data;
      case 'Activity':
        return route.params.data.data;
    }
  };

  const getData = () => {
    axios
      .get(paramApi(route.params.data.title), {
        headers: {
          key: key,
        },
      })
      .then(res => {
        res.data.success ? setData(res.data.data) : false;
      })
      .catch(err => {
        console.log('error', err);
      });
  };

  const renderSub = (dt, item) => {
    // return console.log('aaaz', dt);
    switch (dt) {
      case 'Keilmuan PGB Bangau Putih':
        return navigation.push('BtnList', {data: item});
      case 'Activity':
        return navigation.push('BtnList', {data: item});
      case 'Etika Perguruan':
        return navigation.navigate('AccordionList', {data: item});
    }
  };

  const dataRender = dt => {
    switch (dt) {
      case 'Tentang Kami':
        return dataAboutUs;
      case 'Organisasi':
        return dataOrganization;
      case 'Keilmuan PGB Bangau Putih':
        return route.params.data.data;
      case 'Activity':
        return route.params.data.data;
    }
  };

  const renderList = ({item}) => (
    <TouchableOpacity
      style={{
        flex: 1,
        marginBottom: 16,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Colors.gray6,
      }}
      // onPress={() =>
      //   route.params.data.title === 'Keilmuan PGB Bangau Putih' ||
      //   route.params.data.title === 'Activity'
      //     ? navigation.navigate(item.navigateTo, {
      //         data: {title: item.title, data: item},
      //       })
      //     : item.navigateTo == 'BtnList'
      //     ? navigation.push(item.navigateTo, {data: item})
      //     : navigation.navigate(item.navigateTo, {data: item})
      // }
      onPress={
        () =>
          item.subs?.length > 0
            ? renderSub(item.title, item)
            : navigation.navigate('Description', {data: item})
        // renderSub(route.params.data.title, item)
      }>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingVertical: 20,
          paddingHorizontal: 16,
        }}>
        <Text style={[Font.semiBold14]}>{item.title} </Text>
        <AntDesign
          name="right"
          size={20}
          color={Colors.placholder}
          style={{alignSelf: 'flex-end'}}
        />
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.wrap}>
      <FlatList
        showsVerticalScrollIndicator={false}
        // data={
        //   dataRender(route.params.data.title)
        // }
        data={data}
        renderItem={renderList}
      />
    </View>
  );
};

export default BtnList;

const styles = StyleSheet.create({
  wrap: {flex: 1, backgroundColor: Colors.white, padding: 16},
});
