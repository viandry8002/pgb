import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Colors from '../../../Styles/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Font from '../../../Styles/Fonts';
// import resolveAssetSource from 'resolveAssetSource';
import RenderHtml from 'react-native-render-html';

const windowWidth = Dimensions.get('window').width;

const Description = ({navigation, route}) => {
  useEffect(() => {
    navigation.setOptions({
      title: route.params.data.title,
    });
  }, []);
  const imageSize = Image.resolveAssetSource(route.params.data?.file);

  const titleEmpty = title => {
    if (
      title == 'Silat' ||
      title == 'Tao Kung' ||
      title == 'Sam Po Kun' ||
      title == 'Program Kakilangit' ||
      title == 'PGB Peduli' ||
      title == 'Kie Lin PGB'
    ) {
      return false;
    } else {
      return (
        <View>
          <Text style={[Font.semiBold14]}>{title}</Text>
          <View style={{height: 16}} />
        </View>
      );
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView style={{padding: 16}} showsVerticalScrollIndicator={false}>
        <RenderHtml
          contentWidth={windowWidth - 32}
          tagsStyles={{p: {color: Colors.gray2}, li: {color: Colors.gray2}}}
          source={{
            html: route.params.data?.content,
          }}
        />
        <View style={{height: 16}} />
      </ScrollView>
    </View>
  );
};

export default Description;

const styles = StyleSheet.create({});
