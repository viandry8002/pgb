import {StyleSheet, Text, View, Image} from 'react-native';
import React, {useEffect, useState} from 'react';
import Colors from '../../../Styles/Colors';
import FormInput from '../../Components/FormInput';
import BtnPrimary from '../../Components/BtnPrimary';
import Font from '../../../Styles/Fonts';
import ModalSuccess from '../../Components/ModalSuccess';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {key, postCheckOtp} from '../../../Variable/ApiClient';
import axios from 'axios';
import AlertMsg from '../../Components/AlertMsg';

const Verif = ({navigation, route}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [form, setForm] = useState({
    email: route.params.data.email,
    token: '',
  });
  //   useEffect(() => {
  //     navigation.setOptions({
  //       title: route.params.data.title,
  //     });
  //   }, []);

  const saveToken = async token => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const verification = () => {
    axios
      .post(postCheckOtp, form, {
        headers: {
          key: key,
        },
      })
      .then(res => {
        res.data.success
          ? route.params.type === 'password'
            ? navigation.navigate('BlankForm', {
                data: {title: 'Kata Sandi Baru', data: res.data._id},
              })
            : (saveToken(res.data.token), setModalVisible(true))
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View style={styles.wrap}>
      <View style={{flex: 1}}>
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/Image/verif.png')}
            style={{width: 200, height: 150}}
          />
          <View style={styles.spacing} />
          <Text style={[Font.bold16]}>Kode Verifikasi</Text>
          <Text style={[Font.normal12, {textAlign: 'center', marginTop: 4}]}>
            Kami telah mengirimkan 4 digit kode verifikasi ke {'\n'}
            <Text style={{color: Colors.primary1}}>
              {route.params.data.email}
            </Text>
          </Text>
        </View>
        <View style={styles.spacing} />
        <FormInput
          title={'Masukan Kode'}
          placholder={'Masukan kode'}
          val={form.token}
          change={value => onInputChange(value, 'token')}
        />
        <BtnPrimary
          title={'Lanjut'}
          pressed={
            () => verification()
            // route.params.data.type === 'forgotPass'
            //   ? navigation.navigate('BlankForm', {
            //       data: {title: 'Kata Sandi Baru'},
            //     })
            //   : setModalVisible(true)
          }
        />
      </View>
      <ModalSuccess
        title={'Verifikasi Sukses'}
        desc={'Verifikasi sukses kamu bisa lanjut login ke aplikasi'}
        press={() => (
          setModalVisible(!modalVisible),
          navigation.reset({
            index: 0,
            routes: [{name: 'Home'}],
          })
        )}
        showModal={modalVisible}
      />
    </View>
  );
};

export default Verif;

const styles = StyleSheet.create({
  wrap: {flex: 1, backgroundColor: Colors.white, padding: 16},
  spacing: {height: 16},
});
