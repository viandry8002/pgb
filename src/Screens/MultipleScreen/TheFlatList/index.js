import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, View, Text} from 'react-native';
import Colors from '../../../Styles/Colors';
import {
  getNews,
  getBranch,
  getEvent,
  getMember,
  key,
} from '../../../Variable/ApiClient';
import RenderBranch from '../../Components/RenderBranch';
import RenderEvent from '../../Components/RenderEvent';
import RenderMembers from '../../Components/RenderMembers';
import RenderNews from '../../Components/RenderNews';

const dataCabang = [
  {
    id: 1,
    title: 'Grup Cibinong',
    address:
      'Jl. HR. Lukman No.38, Cirimekar, Kec. Cibinong, Kabupaten Bogor, Jawa Barat,Bogor - Jawa Barat 16917',
    email: 'namaemail@gmail.com',
    pic: 'Agi Widodo',
    phone: '082123123112',
  },
  {
    id: 2,
    title: 'Grup SMP Mardiwaluya Cibinong',
    address:
      'Jl. Mayor Oking Jayaatmaja No. 15, Ciriung Kecamatan/Kota (LN) : Kec. Cibinong Kab.-Kota/Negara (LN) : Kab. Bogor,Bogor - Jawa Barat 16918',
    email: 'namaemail@gmail.com',
    pic: 'Bu Bani',
    phone: '082123123112',
  },
  {
    id: 3,
    title: 'Grup Ciampea',
    address:
      'Pasar Ciampea, Kec. Ciampea, Bogor, Jawa Barat,Bogor - Jawa Barat 16620',
    email: 'namaemail@gmail.com',
    pic: 'Evan',
    phone: '082123123112',
  },
];

const dataInfo = [
  {
    id: 1,
    image: require('../../../Assets/Image/image2.png'),
    title: 'PGB Peduli mengirimkan tim Relawan Erupsi Gunung Semeru',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
  {
    id: 2,
    image: require('../../../Assets/Image/image3.png'),
    title: 'Outbound PGB Bangau Putih',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
  {
    id: 3,
    image: require('../../../Assets/Image/image2.png'),
    title: 'PGB Peduli mengirimkan tim Relawan Erupsi Gunung Semeru',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
  {
    id: 4,
    image: require('../../../Assets/Image/image3.png'),
    title: 'Outbound PGB Bangau Putih',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
  {
    id: 5,
    image: require('../../../Assets/Image/image2.png'),
    title: 'PGB Peduli mengirimkan tim Relawan Erupsi Gunung Semeru',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
  {
    id: 6,
    image: require('../../../Assets/Image/image3.png'),
    title: 'Outbound PGB Bangau Putih',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
];

const dataEvent = [
  {
    id: 1,
    image: require('../../../Assets/Image/Event1.png'),
    title: 'Event Paguyuban Bangau Putih',
    date: 'Selasa, 15 Februari 2022',
    location: 'Kebun Raya',
    address:
      'Jl. Ir. H. Juanda No.13, Paledang, Kecamatan Bogor Tengah, Kota Bogor, Jawa Barat 16122',
  },
  {
    id: 2,
    image: require('../../../Assets/Image/Event2.png'),
    title: 'Event Paguyuban Bangau Putih',
    date: 'Selasa, 15 Februari 2022',
    location: 'Kebun Raya',
    address:
      'Jl. Ir. H. Juanda No.13, Paledang, Kecamatan Bogor Tengah, Kota Bogor, Jawa Barat 16122',
  },
  {
    id: 3,
    image: require('../../../Assets/Image/Event3.png'),
    title: 'Event Paguyuban Bangau Putih',
    date: 'Selasa, 15 Februari 2022',
    location: 'Kebun Raya',
    address:
      'Jl. Ir. H. Juanda No.13, Paledang, Kecamatan Bogor Tengah, Kota Bogor, Jawa Barat 16122',
  },
];

const dataAnggota = [
  {
    id: 1,
    image: require('../../../Assets/Image/person1.png'),
    name: 'Cody Fisher',
  },
  {
    id: 2,
    image: require('../../../Assets/Image/person2.png'),
    name: 'Wade Warren',
  },
  {
    id: 3,
    image: require('../../../Assets/Image/person3.png'),
    name: 'Kathryn Murphy',
  },
  {
    id: 4,
    image: require('../../../Assets/Image/person1.png'),
    name: 'Brooklyn Simmons',
  },
  {
    id: 5,
    image: require('../../../Assets/Image/person2.png'),
    name: 'Ralph Edwards',
  },
  {
    id: 6,
    image: require('../../../Assets/Image/person3.png'),
    name: 'Esther Howard',
  },
];

const TheFlatList = ({navigation, route}) => {
  const [data, setData] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  useEffect(() => {
    navigation.setOptions({
      title: route.params.data.title,
    });
    getData();
  }, []);

  const paramApi = dt => {
    switch (dt) {
      case 'Cabang':
        return getBranch;
      case 'Berita':
        return getNews;
      case 'Acara':
        return getEvent;
      case 'Anggota':
        return getMember;
    }
  };

  const getData = () => {
    axios
      .get(paramApi(route.params.data.title), {
        headers: {
          key: key,
        },
      })
      .then(res => {
        res.data.success
          ? (setData(res.data.data.data),
            res.data.data.next_page_url === null
              ? (sethasScrolled(false), setPass(true))
              : setNextLink(res.data.data.next_page_url))
          : false;
      })
      .catch(err => {
        console.log('error', err);
      });
  };

  const whatRender = (typeMenu, item) => {
    switch (typeMenu) {
      case 'Cabang':
        return <RenderBranch item={item} />;
      case 'Berita':
        return (
          <RenderNews
            item={item}
            navi={() => navigation.navigate('NewsDetail', {data: item})}
          />
        );
      case 'Acara':
        return <RenderEvent item={item} />;
      case 'Anggota':
        return <RenderMembers item={item} />;
    }
  };

  const whatData = typeMenu => {
    switch (typeMenu) {
      case 'Cabang':
        return dataCabang;
      case 'Berita':
        return dataInfo;
      case 'Acara':
        return dataEvent;
      case 'Anggota':
        return dataAnggota;
    }
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    axios
      .get(`${nextLink}`, {
        headers: {
          key: key,
        },
      })
      .then(res => {
        sethasScrolled(!hasScrolled),
          setData([...data, ...res.data.data.data]),
          setNextLink(res.data.data.next_page_url);

        res.data.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <FlatList
        // data={whatData(route.params.data.title)}
        data={data}
        renderItem={({typeMenu = route.params.data.title, item}) =>
          whatRender(typeMenu, item)
        }
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={() => <View style={{height: 16}} />}
        ListFooterComponent={() =>
          hasScrolled === true ? (
            <View style={{alignItems: 'center'}}>
              <Text style={{color: Colors.black}}>Load more ....</Text>
            </View>
          ) : (
            <View style={{height: 16}} />
          )
        }
        onScroll={() => (pass === false ? onScroll() : false)}
        onEndReached={() => (nextLink === null ? false : handleLoadMore())}
      />
    </View>
  );
};

export default TheFlatList;

const styles = StyleSheet.create({});
