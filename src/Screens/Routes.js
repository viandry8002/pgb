import {StyleSheet, Text, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Splashscreen,
  Login,
  Register,
  Provision,
  Home,
  EditProfile,
  BtnList,
  Description,
  AccordionList,
  TheMansoryList,
  TheFlatList,
  MemberCard,
  NewsDetail,
  BlankForm,
  ForgotPassword,
  Verif,
} from './MainScreens';
import Colors from '../Styles/Colors';
import Font from '../Styles/Fonts';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();

const styleHead = title => ({
  title: title,
  headerTitleStyle: [Font.semiBold14, {color: Colors.white, marginLeft: -16}],
  headerTintColor: Colors.white,
  headerBackground: () => (
    <View style={{backgroundColor: Colors.primary1, flex: 1}} />
  ),
});

const MainNavigation = props => {
  return (
    <Stack.Navigator initialRouteName={props.token !== null ? 'Home' : 'Login'}>
      {/* <Stack.Navigator initialRouteName={'Login'}> */}
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Provision"
        component={Provision}
        options={styleHead('Ketentuan Layanan')}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={styleHead('Edit Profile')}
      />
      <Stack.Screen
        name="BtnList"
        component={BtnList}
        options={styleHead('')}
      />
      <Stack.Screen
        name="Description"
        component={Description}
        options={styleHead('')}
      />
      <Stack.Screen
        name="AccordionList"
        component={AccordionList}
        options={styleHead('')}
      />
      <Stack.Screen
        name="TheMansoryList"
        component={TheMansoryList}
        options={styleHead('')}
      />
      <Stack.Screen
        name="TheFlatList"
        component={TheFlatList}
        options={styleHead('')}
      />
      <Stack.Screen
        name="MemberCard"
        component={MemberCard}
        options={styleHead('Kartu Anggota')}
      />
      <Stack.Screen
        name="NewsDetail"
        component={NewsDetail}
        options={styleHead('Detail Berita')}
      />
      <Stack.Screen
        name="BlankForm"
        component={BlankForm}
        options={styleHead('')}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Verif"
        component={Verif}
        options={styleHead('Verifikasi OTP')}
      />
    </Stack.Navigator>
  );
};

const Routes = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [authtoken, setAuthToken] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);

    async function getToken() {
      const token = await AsyncStorage.getItem('api_token');
      setAuthToken(token);
    }
    getToken();
  }, []);

  if (isLoading) {
    return <Splashscreen />;
  }

  return (
    <NavigationContainer>
      <MainNavigation token={authtoken} />
    </NavigationContainer>
  );
};

export default Routes;

const styles = StyleSheet.create({});
