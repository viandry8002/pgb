import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {SliderBox} from 'react-native-image-slider-box';
import RenderEvent from '../../Components/RenderEvent';
import axios from 'axios';
import {
  key,
  getMe,
  getSliders,
  getNews,
  getConfiguration,
} from '../../../Variable/ApiClient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SwiperFlatList from 'react-native-swiper-flatlist';
import moment from 'moment';
import RenderHtml from 'react-native-render-html';

const windowWidth = Dimensions.get('window').width;

const dataSlider = [
  require('../../../Assets/Image/image1.png'),
  require('../../../Assets/Image/image2.png'),
  require('../../../Assets/Image/image3.png'),
];

const dataMenu = [
  {
    id: 1,
    image: require('../../../Assets/Icon/logo.png'),
    title: 'Tentang Kami',
    navigateTo: 'BtnList',
  },
  {
    id: 2,
    image: require('../../../Assets/Icon/organization.png'),
    title: 'Organisasi',
    navigateTo: 'BtnList',
  },
  {
    id: 3,
    image: require('../../../Assets/Icon/gallery.png'),
    title: 'Galeri',
    navigateTo: 'TheMansoryList',
  },
  {
    id: 4,
    image: require('../../../Assets/Icon/branch.png'),
    title: 'Cabang',
    navigateTo: 'TheFlatList',
  },
  {
    id: 5,
    image: require('../../../Assets/Icon/news.png'),
    title: 'Berita',
    navigateTo: 'TheFlatList',
  },
  {
    id: 6,
    image: require('../../../Assets/Icon/magazine.png'),
    title: 'Majalah',
    navigateTo: 'TheMansoryList',
  },
  {
    id: 7,
    image: require('../../../Assets/Icon/event.png'),
    title: 'Acara',
    navigateTo: 'TheFlatList',
  },
  {
    id: 8,
    image: require('../../../Assets/Icon/member.png'),
    title: 'Anggota',
    navigateTo: 'TheFlatList',
  },
];

const dataInfo = [
  {
    id: 1,
    image: require('../../../Assets/Image/image2.png'),
    title: 'PGB Peduli mengirimkan tim Relawan Erupsi Gunung Semeru',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
  {
    id: 2,
    image: require('../../../Assets/Image/image3.png'),
    title: 'Outbound PGB Bangau Putih',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
    create_at: '14 Feb 2022',
  },
];

const Home = ({navigation}) => {
  const [config, setConfig] = useState([]);
  const [user, setUser] = useState([]);
  const [slider, setSlider] = useState([]);
  const [news, setNews] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [AuthToken, setAuthToken] = useState('');

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return home(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const home = token => {
    setRefreshing(true);
    setTimeout(() => {
      setAuthToken(token);
      // get config
      axios
        .get(getConfiguration, {
          headers: {
            key: key,
          },
        })
        .then(res => {
          res.data.success ? setConfig(res.data.data) : false;
        })
        .catch(err => {
          console.log('error', err.response);
        });

      // get me
      axios
        .get(getMe, {
          headers: {
            key: key,
            Authorization: 'Bearer ' + token,
          },
        })
        .then(res => {
          res.data.success ? setUser(res.data.data) : false;
        })
        .catch(err => {
          console.log('error', err.response);
          err.response.data === 'Unauthorized.'
            ? navigation.reset({
                index: 0,
                routes: [{name: 'Login'}],
              })
            : false;
        });

      // get slider
      axios
        .get(getSliders, {
          headers: {
            key: key,
          },
        })
        .then(res => {
          res.data.success ? setSlider(res.data.data) : false;
        })
        .catch(err => {
          console.log('error', err);
        });

      // get news
      axios
        .get(getNews + '?limit=5', {
          headers: {
            key: key,
          },
        })
        .then(res => {
          res.data.success ? setNews(res.data.data.data) : false;
        })
        .catch(err => {
          console.log('error', err);
        });
      setRefreshing(false);
    }, 1000);
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      home(AuthToken);
    }, 1000);
  };

  const renderMenu = (item, index) => {
    return (
      <TouchableOpacity
        key={index}
        style={{
          marginVertical: 8,
          alignItems: 'center',
          width: '25%',
        }}
        onPress={() => navigation.navigate(item.navigateTo, {data: item})}>
        <View
          style={{
            width: 64,
            height: 64,
            borderRadius: 16,
            borderWidth: 1,
            borderColor: Colors.gray6,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={item.image}
            style={{
              width: 42,
              height: 42,
            }}
          />
        </View>
        <View style={{height: 8}} />
        <Text style={[Font.normal12, {textAlign: 'center', width: 60}]}>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderInfo = (item, index) => {
    return (
      <TouchableOpacity
        key={index}
        style={{
          flex: 1,
          marginHorizontal: 16,
          borderRadius: 16,
          marginBottom: 8,
          elevation: 2,
        }}
        onPress={() => navigation.navigate('NewsDetail', {data: item})}>
        <Image
          source={{uri: item.file}}
          style={{
            width: '100%',
            height: 145,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
          }}
        />
        <View
          style={{
            flex: 1,
            padding: 8,
            backgroundColor: Colors.white,
            borderBottomLeftRadius: 16,
            borderBottomRightRadius: 16,
          }}>
          <Text style={[Font.semiBold14]}>{item.title}</Text>
          <View style={{height: 8}} />
          {/* <Text style={[Font.normal12, {color: Colors.placholder}]}>
            {item.description}
          </Text> */}
          <RenderHtml
            contentWidth={windowWidth - 32}
            tagsStyles={{p: {color: Colors.gray2}, li: {color: Colors.gray2}}}
            source={{
              html: item.description,
            }}
            defaultTextProps={{numberOfLines: 2}}
            enableExperimentalGhostLinesPrevention={true}
            enableExperimentalMarginCollapsing={true}
          />
          <View style={{height: 8}} />
          <Text style={[Font.normal12]}>
            {moment(item.created_at).format('DD MMM YYYY')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <View style={{height: 20}} />
      <ScrollView
        style={{flex: 1}}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <StatusBar backgroundColor={Colors.primary1} translucent />
        <View style={styles.redBg} />
        <TouchableOpacity
          onPress={() => navigation.navigate('EditProfile')}
          style={styles.wrapProfile}>
          <Image
            source={
              user?.file == null
                ? require('../../../Assets/Icon/person.png')
                : {uri: user.file}
            }
            style={styles.imageProfile}
          />
          <View style={{flex: 1}}>
            <Text style={[Font.normal12, {color: Colors.white}]}>
              {config.title_slogan}
            </Text>
            <Text style={[Font.semiBold20, {color: Colors.white}]}>
              {user?.name}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={{alignItems: 'center'}}>
          <View style={styles.wrapSlider}>
            {/* <SliderBox
              autoplay
              circleLoop
              images={dataSlider}
              ImageComponentStyle={{
                borderRadius: 15,
                height: 194,
                width: windowWidth - 32,
              }}
              paginationBoxStyle={{
                alignSelf: 'flex-start',
              }}
              dotStyle={{
                width: 8,
                height: 8,
                marginHorizontal: -20,
                left: -20,
              }}
              dotColor={Colors.primary1}
              inactiveDotColor={Colors.gray6}
              parentWidth={windowWidth - 32}
            /> */}
            <SwiperFlatList
              paginationBoxVerticalPadding={10}
              autoplay
              autoplayDelay={2}
              autoplayLoop
              // index={2}
              showPagination
              data={slider}
              renderItem={({item}) => (
                <Image
                  source={{uri: item.file}}
                  style={{
                    width: windowWidth - 32,
                    height: 194,
                    borderRadius: 15,
                  }}
                />
              )}
              paginationStyleItem={{
                width: 8,
                height: 8,
                marginTop: 8,
                marginHorizontal: 4,
              }}
              paginationStyle={{
                bottom: 0,
                alignSelf: 'flex-start',
                marginHorizontal: 16,
              }}
              paginationStyleItemActive={{backgroundColor: Colors.primary1}}
              paginationStyleItemInactive={{
                backgroundColor: Colors.gray6,
              }}
            />
          </View>
        </View>
        <View style={{flex: 1, marginTop: 8}}>
          <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
            {dataMenu.map((item, index) => renderMenu(item, index))}
          </View>
        </View>
        <View style={{height: 16}} />
        {/* info terbaru */}
        <View
          style={{
            flex: 1,
            marginHorizontal: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={[Font.bold16]}>Info Terbaru</Text>
          <TouchableOpacity
            style={{
              paddingVertical: 8,
              paddingHorizontal: 12,
              backgroundColor: Colors.lightPrimary,
              borderRadius: 100,
            }}
            onPress={() =>
              navigation.navigate('TheFlatList', {data: {title: 'Berita'}})
            }>
            <Text style={[Font.semiBold12, {color: Colors.primary1}]}>
              Lihat Semua
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{height: 8}} />
        {news.map((item, index) => renderInfo(item, index))}
        <View style={{height: 16}} />
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  spacing: {
    height: 16,
  },
  wrapWelcome: {height: 158, padding: 16, flexDirection: 'row'},
  redBg: {
    backgroundColor: Colors.primary1,
    height: 192,
    width: windowWidth,
    position: 'absolute',
  },
  wrapProfile: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 16,
    marginTop: 48,
    alignItems: 'center',
  },
  imageProfile: {width: 45, height: 45, marginRight: 10, borderRadius: 100},
  wrapSlider: {paddingHorizontal: 16, height: 220, marginTop: 16},
});
