import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import React from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import moment from 'moment';
import RenderHtml from 'react-native-render-html';

const windowWidth = Dimensions.get('window').width;

const NewsDetail = ({route}) => {
  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Image
          source={{uri: route.params.data.file}}
          style={{width: windowWidth, height: 216}}
        />
        <View style={{flex: 1, padding: 16}}>
          <Text style={[Font.bold16]}>{route.params.data.title}</Text>
          <View style={{height: 8}} />
          <Text style={[Font.normal12]}>
            {' '}
            {moment(route.params.data.created_at).format('DD MMM YYYY')}
          </Text>
          <View
            style={{
              marginVertical: 8,
              borderBottomColor: Colors.gray6,
              borderBottomWidth: 1,
            }}
          />
          {/* <Text style={[Font.normal14]}>{route.params.data.description}</Text> */}
          <RenderHtml
            contentWidth={windowWidth - 32}
            tagsStyles={{p: {color: Colors.gray2}, li: {color: Colors.gray2}}}
            source={{
              html: route.params.data.description,
            }}
          />
          {/* <Text style={[Font.normal14]}>
            Lorem ipsum dolor sit amet consectetur. Vitae massa nulla purus
            lacus at diam et. Tempus facilisis laoreet ultrices iaculis odio
            justo orci mattis viverra. Sed commodo id nisl ut. Urna aenean
            suscipit sed gravida tortor eget. Nunc ultricies elementum nec
            mattis velit varius. Ut facilisis a risus et at bibendum. Urna
            pretium sodales sed at accumsan. Eget habitasse placerat sed vitae.
            Diam nulla turpis elementum habitant congue ut volutpat quis lacus.
            Laoreet senectus pharetra hendrerit turpis amet integer.
          </Text>
          <Text style={[Font.normal14]}>
            Lorem ipsum dolor sit amet consectetur. Vitae massa nulla purus
            lacus at diam et. Tempus facilisis laoreet ultrices iaculis odio
            justo orci mattis viverra. Sed commodo id nisl ut. Urna aenean
            suscipit sed gravida tortor eget. Nunc ultricies elementum nec
            mattis velit varius. Ut facilisis a risus et at bibendum. Urna
            pretium sodales sed at accumsan. Eget habitasse placerat sed vitae.
            Diam nulla turpis elementum habitant congue ut volutpat quis lacus.
            Laoreet senectus pharetra hendrerit turpis amet integer.
          </Text> */}
        </View>
      </ScrollView>
    </View>
  );
};

export default NewsDetail;

const styles = StyleSheet.create({});
