import React, {useEffect, useRef} from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  SafeAreaView,
  Animated,
  ImageBackground,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../../Styles/Colors';

const Splashscreen = () => {
  const fadeIn = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  }, [fadeIn]);

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="transparent" translucent />
      <LinearGradient
        colors={[Colors.primary1, Colors.primary2]}
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Animated.View style={{opacity: fadeIn}}>
          <Image
            source={require('../../../Assets/Image/logo.png')}
            style={{width: 176, height: 176}}
          />
        </Animated.View>
      </LinearGradient>
    </View>
  );
};

export default Splashscreen;

const styles = StyleSheet.create({});
