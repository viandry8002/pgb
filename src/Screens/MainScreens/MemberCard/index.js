import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ToastAndroid,
  PermissionsAndroid,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import BtnPrimary from '../../Components/BtnPrimary';
import Feather from 'react-native-vector-icons/Feather';
import axios from 'axios';
import {key, getCardMember} from '../../../Variable/ApiClient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNFetchBlob from 'rn-fetch-blob';
import ToastMsg from '../../Components/ToastMsg';

const windowWidth = Dimensions.get('window').width;

const MemberCard = ({route}) => {
  const [linkCard, setLinkCard] = useState('');

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getCard(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const getCard = token => {
    axios
      .get(getCardMember, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        res.data.success ? setLinkCard(res.data.url) : false;
      })
      .catch(err => {
        console.log('error', err);
      });
  };

  const requestStoragePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        actualDownload();
      } else {
        // Alert.alert('Peringatan', 'Izinkan File dan media terlebih dahulu', [
        //   {
        //     text: 'ok',
        //     onPress: () => console.log('Tidak'),
        //   },
        // ]);
        console.log('External storage permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const actualDownload = () => {
    const {dirs} = RNFetchBlob.fs;
    RNFetchBlob.config({
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: `member.pdf`,
        path: `${dirs.DownloadDir}/member.pdf`,
      },
    })
      .fetch('GET', linkCard, {})
      .then(res => {
        // console.log('The file saved to ', res.path());
        ToastMsg('Kartu Member berhasil di simpan');
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <View style={{flex: 1, padding: 16}}>
      <ImageBackground
        source={require('../../../Assets/Image/memberCard.png')}
        style={{width: windowWidth - 32, height: 520}}
        resizeMode={'contain'}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            top: 120,
          }}>
          <Image
            source={
              route.params?.data.file === null
                ? require('../../../Assets/Icon/person.png')
                : {uri: route.params?.data.file}
            }
            style={{width: 180, height: 240}}
          />
          <View style={{height: 8}} />
          <Text style={[Font.semiBold24, {color: Colors.white}]}>
            {route.params?.data.name}
          </Text>
          <View
            style={{
              width: 200,
              marginVertical: 4,
              borderBottomColor: Colors.white,
              borderBottomWidth: 1,
            }}
          />
          <Text style={[Font.semiBold16, {color: Colors.white}]}>
            {route.params?.data.branch_name}
          </Text>
          <Text style={[Font.semiBold14, {color: Colors.white}]}>
            {route.params?.data.belt_status}
          </Text>
        </View>
      </ImageBackground>
      <View style={{height: 50}} />
      <BtnPrimary
        leftIcon={
          <Feather
            name={'download'}
            size={20}
            style={{color: Colors.white, marginRight: 10}}
          />
        }
        title={'Download PDF'}
        pressed={() => requestStoragePermission()}
      />
    </View>
  );
};

export default MemberCard;

const styles = StyleSheet.create({});
