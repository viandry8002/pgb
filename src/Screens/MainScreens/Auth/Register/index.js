import {CheckBox} from '@rneui/themed';
import axios from 'axios';
import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {key, postRegister} from '../../../../Variable/ApiClient';
import AlertMsg from '../../../Components/AlertMsg';
import BtnPrimary from '../../../Components/BtnPrimary';
import FormInput from '../../../Components/FormInput';

const Register = ({navigation}) => {
  const [form, setForm] = useState({
    name: '',
    username: '',
    email: '',
    password: '',
    password_confirmation: '',
  });
  const [check, setCheck] = useState(false);

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const register = () => {
    check
      ? axios
          .post(postRegister, form, {
            headers: {
              key: key,
            },
          })
          .then(res => {
            res.data.success
              ? res.data.message === 'Kode OTP berhasil dikirim'
                ? navigation.navigate('Verif', {
                    data: res.data.create,
                    type: 'normal',
                  })
                : (saveToken(res.data.token),
                  navigation.reset({
                    index: 0,
                    routes: [{name: 'Home'}],
                  }))
              : AlertMsg(res.data.message);
          })
          .catch(err => {
            console.log(err);
          })
      : AlertMsg('Klik menyetujui ketentuan terlebih dahulu');
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.primary1}}>
      <StatusBar backgroundColor="transparent" translucent />
      <View style={{height: 84}} />
      <View style={styles.wrapWelcome}>
        <View style={{flex: 1}}>
          <Text style={[Font.bold24, {color: Colors.white}]}>
            Buat Akun {'\n'}Member Baru
          </Text>
          <View style={{height: 6}} />
          <Text style={[Font.normal12, {color: Colors.white}]}>
            Buat akun Member baru {'\n'}Paguyuban Bangau Putih
          </Text>
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Image
            source={require('../../../../Assets/Image/logo.png')}
            style={{width: 100, height: 100}}
          />
        </View>
      </View>
      <View style={styles.wrapForm}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <FormInput
            title={'Nama Lengkap'}
            placholder={'Masukan nama lengkap'}
            icon={false}
            val={form.name}
            change={value => onInputChange(value, 'name')}
          />
          <FormInput
            title={'Username'}
            placholder={'Masukan username'}
            icon={false}
            val={form.username}
            change={value => onInputChange(value, 'username')}
          />
          <FormInput
            title={'Alamat Email'}
            placholder={'Masukan alamat email'}
            icon={false}
            val={form.email}
            change={value => onInputChange(value, 'email')}
          />
          <FormInput
            title={'Kata Sandi'}
            placholder={'Masukan sandi'}
            icon={false}
            val={form.password}
            change={value => onInputChange(value, 'password')}
            scureText={true}
          />
          <FormInput
            title={'Konfirmasi Kata Sandi'}
            placholder={'Masukan ulang kata sandi'}
            icon={false}
            val={form.password_confirmation}
            change={value => onInputChange(value, 'password_confirmation')}
            scureText={true}
          />
          <View style={{flex: 1, flexDirection: 'row'}}>
            <CheckBox
              containerStyle={{padding: 0}}
              checked={check}
              onPress={() => setCheck(!check)}
              uncheckedColor={Colors.gray1}
              checkedColor={Colors.gray1}
            />
            <Text style={[Font.normal12, {flex: 1}]}>
              Saya sudah membaca dan menyetujui
              <Text
                style={[Font.semiBold12, {color: Colors.primary1}]}
                onPress={() => navigation.navigate('Provision')}>
                {' '}
                Ketentuan Layanan Perguruan Gerak Badan Bangau Putih
              </Text>
            </Text>
          </View>
          <View style={styles.spacing} />
          <BtnPrimary
            title={'Daftar'}
            pressed={
              () => register()
              // navigation.navigate('Verif', {
              //   data: {title: '', type: 'register'},
              // })
            }
          />
          <View style={styles.spacing} />
          <Text style={[Font.normal12, {alignSelf: 'center', flex: 1}]}>
            Sudah punya akun?{' '}
            <Text
              style={[Font.semiBold12, {color: Colors.primary1}]}
              onPress={() => navigation.navigate('Login')}>
              Login disini
            </Text>
          </Text>
        </ScrollView>
      </View>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  spacing: {
    height: 16,
  },
  wrapWelcome: {height: 158, padding: 16, flexDirection: 'row'},
  wrapForm: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    padding: 16,
  },
});
