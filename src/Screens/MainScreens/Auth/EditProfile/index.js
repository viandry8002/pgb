import {CheckBox} from '@rneui/themed';
import React, {useState, useEffect} from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import BtnPrimary from '../../../Components/BtnPrimary';
import FormInput from '../../../Components/FormInput';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import BtnOutline from '../../../Components/BtnOutline';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  key,
  getMe,
  postUpdateProfile,
  postLogout,
} from '../../../../Variable/ApiClient';
import axios from 'axios';
import AlertMsg from '../../../Components/AlertMsg';
import ToastMsg from '../../../Components/ToastMsg';
import SpinnerLoad from '../../../Components/SpinnerLoad';
import {launchImageLibrary} from 'react-native-image-picker';

const EditProfile = ({navigation}) => {
  const [form, setForm] = useState({
    file: '',
    name: '',
    username: '',
    email: '',
    branch_name: '',
    status_belt: '',
  });
  const [tokenAuth, setTokenAuth] = useState('');
  const [load, setLoad] = useState(false);
  const [isChoseFile, setIsChoseFile] = useState(false);
  const [image, setImage] = useState(null);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getProfile(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const getProfile = token => {
    axios
      .get(getMe, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + token,
        },
      })
      .then(res => {
        res.data.success
          ? (setForm({
              ...form,
              ['file']: res.data.data.file,
              ['name']: res.data.data.name,
              ['username']: res.data.data.username,
              ['email']: res.data.data.email,
              ['branch_name']: res.data.data.cabang_name,
              ['belt_status']: res.data.data.status_sabuk,
            }),
            setTokenAuth(token))
          : false;
      })
      .catch(err => {
        console.log('error', err);
      });
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 3000,
      maxHeight: 3000,
      quality: 1,
    };
    launchImageLibrary(options, response => {
      console.log('aaaayyyyy', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        // alert(response.customButton);
      } else {
        let tmpFiles = {
          uri: response.assets[0]?.uri,
          type: response.assets[0]?.type,
          name: response.assets[0]?.fileName,
        };
        setIsChoseFile(true);
        setImage(response?.assets[0]?.uri);
        onInputChange(tmpFiles, 'file');
      }

      // onInputChange(tmpFiles, 'file');
      // onUpdateProfile(tmpFiles)
    });
  };

  const updateProfile = () => {
    const formsdata = new FormData();

    formsdata.append('name', form.name);
    formsdata.append('username', form.username);
    formsdata.append('email', form.email);
    isChoseFile
      ? formsdata.append('file', form.file)
      : formsdata.append('file', '');

    axios
      .post(postUpdateProfile, formsdata, {
        headers: {
          'Content-Type': 'multipart/form-data',
          key: key,
          Authorization: 'Bearer ' + tokenAuth,
        },
      })
      .then(res => {
        res.data.success
          ? (ToastMsg('sukses merubah data'), getProfile(tokenAuth))
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const navTologout = () => {
    setLoad(true);
    setTimeout(() => {
      axios
        .post(
          postLogout,
          {},
          {
            headers: {
              key: key,
              Authorization: 'Bearer ' + tokenAuth,
            },
          },
        )
        .then(res => {
          setLoad(false);
          res.data.success
            ? (ToastMsg(res.data.data),
              navigation.reset({
                index: 0,
                routes: [{name: 'Login'}],
              }))
            : AlertMsg(res.data.message);
        })
        .catch(err => {
          setLoad(false);
          console.log(err);
        });
    }, 100);
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.white, padding: 16}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <SpinnerLoad loads={load} />
        {/* Image profile */}
        <View style={{alignSelf: 'center'}}>
          <Image
            source={
              // form.file === null
              //   ? require('../../../../Assets/Icon/person.png')
              //   : {uri: form.file}
              form.file
                ? isChoseFile === false
                  ? {uri: form.file}
                  : {uri: image}
                : require('../../../../Assets/Icon/person.png')
            }
            style={{width: 96, height: 96, borderRadius: 100}}
          />
          <TouchableOpacity
            style={styles.wrapCamera}
            onPress={() => chooseFile('photo')}>
            <FontAwesome5 name="camera" size={18} color={Colors.white} />
          </TouchableOpacity>
        </View>
        {/* member card */}
        <View style={{height: 34}} />
        <BtnOutline
          pressed={() => navigation.navigate('MemberCard', {data: form})}
          title={'Lihat Kartu Anggota'}
          leftIcon={
            <AntDesign name="idcard" size={24} color={Colors.primary1} />
          }
        />
        {/* form */}
        <View style={{height: 16}} />
        <FormInput
          title={'Nama Lengkap'}
          placholder={'Masukan nama lengkap'}
          icon={false}
          val={form.name}
          change={value => onInputChange(value, 'name')}
        />
        <FormInput
          title={'Username'}
          placholder={'Masukan username'}
          icon={false}
          val={form.username}
          change={value => onInputChange(value, 'username')}
        />
        <FormInput
          title={'Alamat Email'}
          placholder={'Masukan alamat email'}
          icon={false}
          val={form.email}
          change={value => onInputChange(value, 'email')}
        />
        <Text
          style={[
            Font.semiBold12,
            {color: Colors.primary1, alignSelf: 'flex-end'},
          ]}
          onPress={() =>
            navigation.navigate('BlankForm', {
              data: {title: 'Password Baru'},
            })
          }>
          Ganti Password
        </Text>
        <View style={{height: 16}} />
        <BtnPrimary
          title={'Simpan Perubahan'}
          pressed={() => updateProfile()}
        />
        <View style={{height: 16}} />
        <BtnPrimary
          title={'Log Out'}
          leftIcon={
            <SimpleLineIcons
              name="logout"
              size={20}
              color={Colors.primary1}
              style={{marginRight: 10}}
            />
          }
          colorTint={Colors.primary1}
          bgColor={Colors.lightPrimary}
          pressed={() => navTologout()}
        />
      </ScrollView>
    </View>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  wrapCamera: {
    width: 34,
    height: 34,
    borderRadius: 100,
    position: 'absolute',
    backgroundColor: Colors.primary1,
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    right: 0,
  },
});
