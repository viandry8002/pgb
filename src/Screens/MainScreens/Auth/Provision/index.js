import {ScrollView, StyleSheet, Text, Dimensions, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {key, getConfiguration} from '../../../../Variable/ApiClient';
import RenderHtml from 'react-native-render-html';

const windowWidth = Dimensions.get('window').width;

const Provision = () => {
  const [Ketentuan, setKetentuan] = useState('');

  useEffect(() => {
    return ketentuan();
  }, []);

  const ketentuan = () => {
    axios
      .get(getConfiguration, {
        headers: {
          key: key,
        },
      })
      .then(res => {
        res.data.success ? setKetentuan(res.data.data.s_k) : false;
      })
      .catch(err => {
        console.log('error', err);
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView style={{flex: 1, margin: 16}}>
        {/* <Text style={Font.bold16}>Kunjungan Murid ke Pusat Perguruan</Text>
        <View style={styles.spacing} />
        <Text style={Font.normal12}>
          Untuk anggota PGB Bangau Putih dari wilayah Indonesia, yang akan
          mengunjungi Pusat Perguruan, untuk tinggal dan berlatih, diwajibkan
          mengikuti peraturan yang tercantum dibawah ini, yaitu:{'\n'}
          {'\n'}1. Menulis surat permohonan kepada Dewan Keorganisasian yang
          telah diketahui dan ditandatangi oleh orang tua, pengurus cabang atau
          grup, serta pelatih dan mengirimkan melalui email atau pos (cantumkan
          nomor kontak yang bisa dihubungi.)
          {'\n'}2. Tunggu jawaban dari Pusat, dan jawaban akan dikirim melalui
          email atau telepon langsung.
          {'\n'}3. Jika diperkenankan, silahkan langsung datang ke Pusat
          Perguruan.
        </Text>
        <View style={styles.spacing} />
        <Text style={Font.bold16}>Keterangan lain:</Text>
        <View style={styles.spacing} />
        <Text style={Font.normal12}>
          - Jika anggota datang berkunjung ke PGB Bangau Putih Pusat Bogor,
          hanya untuk berlatih, diharuskan membawa surat keterangan dari
          pengurus cabang, grup dan disetujui oleh pelatih.
          {'\n'}- Jika pengurus cabang beserta anggotanya akan berkunjung ke PGB
          Bangau Putih Pusat Bogor, silahkan mengirim surat permohonan kunjungan
          beserta maksud dan tujuannya ke Organisasi Pusat.
        </Text> */}
        {/* <Text style={Font.normal12}>{Ketentuan}</Text> */}
        <RenderHtml
          contentWidth={windowWidth - 32}
          tagsStyles={{p: {color: Colors.gray2}, li: {color: Colors.gray2}}}
          source={{
            html: Ketentuan,
          }}
        />
      </ScrollView>
    </View>
  );
};

export default Provision;

const styles = StyleSheet.create({
  spacing: {
    height: 16,
  },
});
