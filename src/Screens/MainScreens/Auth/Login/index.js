import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useState} from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../../Styles/Colors';
import Font from '../../../../Styles/Fonts';
import {key, postLogin} from '../../../../Variable/ApiClient';
import AlertMsg from '../../../Components/AlertMsg';
import BtnPrimary from '../../../Components/BtnPrimary';
import FormInput from '../../../Components/FormInput';
import SpinnerLoad from '../../../Components/SpinnerLoad';

const Login = ({navigation}) => {
  const [load, setLoad] = useState(false);
  const [form, setForm] = useState({
    username: '',
    password: '',
  });
  const [show, setShow] = useState(true);

  // const saveToken = async token => {
  //   try {
  //     if (token !== null) {
  //       await AsyncStorage.setItem('api_token', token);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  const saveToken = async token => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const login = () => {
    setLoad(true);
    setTimeout(() => {
      axios
        .post(postLogin, form, {
          headers: {
            key: key,
          },
        })
        .then(res => {
          setLoad(false);
          res.data.success
            ? res.data.message === 'Kode OTP berhasil dikirim'
              ? navigation.navigate('Verif', {
                  data: res.data.create,
                  type: 'normal',
                })
              : (saveToken(res.data.token),
                navigation.reset({
                  index: 0,
                  routes: [{name: 'Home'}],
                }))
            : AlertMsg(res.data.message);
        })
        .catch(err => {
          setLoad(false);
          console.log(err);
        });
    }, 100);
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.primary1}}>
      <StatusBar backgroundColor="transparent" translucent />
      <SpinnerLoad loads={load} />
      <View style={{height: 84}} />
      <View style={styles.wrapWelcome}>
        <View style={{flex: 1}}>
          <Text style={[Font.bold24, {color: Colors.white}]}>
            Selamat {'\n'}Datang Kembali
          </Text>
          <View style={{height: 6}} />
          <Text style={[Font.normal12, {color: Colors.white}]}>
            Selamat Datang kembali di {'\n'}aplikasi Paguyuban Bangau Putih
          </Text>
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Image
            source={require('../../../../Assets/Image/logo.png')}
            style={{width: 100, height: 100}}
          />
        </View>
      </View>
      <View style={styles.wrapForm}>
        <FormInput
          title={'Username'}
          placholder={'Masukan username'}
          icon={<Ionicons name="person" size={20} color={Colors.primary1} />}
          val={form.username}
          change={value => onInputChange(value, 'username')}
        />
        <FormInput
          title={'Kata Sandi'}
          placholder={'Masukan kata sandi'}
          icon={<Fontisto name="locked" size={20} color={Colors.primary1} />}
          rightIcon={
            <MaterialCommunityIcons
              onPress={() => setShow(!show)}
              name={show === true ? 'eye-off-outline' : 'eye-outline'}
              size={20}
              color={Colors.black}
            />
          }
          scureText={show}
          val={form.password}
          change={value => onInputChange(value, 'password')}
        />
        <Text
          style={[
            Font.normal12,
            {color: Colors.primary1, alignSelf: 'flex-end'},
          ]}
          onPress={() => navigation.navigate('ForgotPassword')}>
          Lupa Kata Sandi??
        </Text>
        <View style={styles.spacing} />
        <BtnPrimary
          title={'Login'}
          pressed={() => login()}
          // login
          // pressed={() => navigation.navigate('Home')}
        />
        <View style={styles.spacing} />
        <Text style={[Font.normal12, {alignSelf: 'center'}]}>
          Belum punya akun?{' '}
          <Text
            style={[Font.semiBold12, {color: Colors.primary1}]}
            onPress={() => navigation.navigate('Register')}>
            Daftar disini
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  spacing: {
    height: 16,
  },
  wrapWelcome: {height: 158, padding: 16, flexDirection: 'row'},
  wrapForm: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    padding: 16,
  },
});
