import React, {useEffect, useState, useRef} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import Colors from '../../../../Styles/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Font from '../../../../Styles/Fonts';
import FormInput from '../../../Components/FormInput';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import BtnPrimary from '../../../Components/BtnPrimary';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {key, postSendOtp} from '../../../../Variable/ApiClient';
import AlertMsg from '../../../Components/AlertMsg';

const ForgotPassword = ({navigation}) => {
  const [form, setForm] = useState({
    email: '',
  });

  const forget = () => {
    axios
      .post(postSendOtp, form, {
        headers: {
          key: key,
        },
      })
      .then(res => {
        res.data.success
          ? navigation.navigate('Verif', {
              data: {email: form.email},
              type: 'password',
            })
          : AlertMsg(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.primary1}}>
      <StatusBar backgroundColor="transparent" translucent />
      <View style={{height: 84}} />
      <View style={styles.wrapWelcome}>
        <View style={{flex: 1}}>
          <Text style={[Font.bold24, {color: Colors.white}]}>
            Lupa Kata Sandi
          </Text>
          <View style={{height: 6}} />
          <Text style={[Font.normal12, {color: Colors.white}]}>
            Masukan email anda untuk me- reset Kata Sandi anda
          </Text>
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Image
            source={require('../../../../Assets/Image/logo.png')}
            style={{width: 100, height: 100}}
          />
        </View>
      </View>
      <View style={styles.wrapForm}>
        <FormInput
          title={'Email'}
          placholder={'Masukan email'}
          icon={
            <MaterialCommunityIcons
              name="email"
              size={20}
              color={Colors.primary1}
            />
          }
          val={form.token}
          change={value =>
            setForm({
              ...form,
              ['email']: value,
            })
          }
        />
        <View style={styles.spacing} />
        <BtnPrimary title={'Lanjut'} pressed={() => forget()} />
      </View>
    </View>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  spacing: {
    height: 16,
  },
  wrapWelcome: {height: 158, padding: 16, flexDirection: 'row'},
  wrapForm: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    padding: 16,
  },
});
