import Splashscreen from './Splashscreen';
import Login from './Auth/Login';
import Register from './Auth/Register';
import Provision from './Auth/Provision';
import Home from './Home';
import EditProfile from './Auth/EditProfile';
import MemberCard from './MemberCard';
import NewsDetail from './NewsDetail';
import ForgotPassword from './Auth/ForgotPassword';
// Multiple Screen
import BtnList from '../MultipleScreen/BtnList';
import Description from '../MultipleScreen/Description';
import AccordionList from '../MultipleScreen/AccordionList';
import TheMansoryList from '../MultipleScreen/TheMasonryList';
import TheFlatList from '../MultipleScreen/TheFlatList';
import BlankForm from '../MultipleScreen/BlankForm';
import Verif from '../MultipleScreen/Verif';

export {
  Splashscreen,
  Login,
  Register,
  Provision,
  Home,
  EditProfile,
  BtnList,
  Description,
  AccordionList,
  TheMansoryList,
  TheFlatList,
  MemberCard,
  NewsDetail,
  BlankForm,
  ForgotPassword,
  Verif,
};
