import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'

export const screenWidth = Dimensions.get('screen').width;
export const screenWidthMargin = Dimensions.get('screen').width - 32;
export const screenHeight = Dimensions.get('screen').height;