const Colors = {
  primary1: '#C51A1A',
  primary2: '#2F0606',
  lightPrimary: '#FCEBED',
  black: '#000000',
  white: '#FFFFFF',
  placholder: '#79747E',
  gray1: '#333333',
  gray2: '#4F4F4F',
  gray6: '#F2F2F2',
};

export default Colors;
