import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from './Colors';

const Font = StyleSheet.create({
  // bold
  bold24: {
    fontFamily: 'Inter-Regular',
    fontWeight: '700',
    fontSize: 24,
    color: Colors.black,
  },
  bold16: {
    fontFamily: 'Inter-Regular',
    fontWeight: '700',
    fontSize: 16,
    color: Colors.black,
  },
  // semi bold
  semiBold24: {
    fontFamily: 'Inter-SemiBold',
    fontWeight: '500',
    fontSize: 24,
    color: Colors.black,
  },
  semiBold20: {
    fontFamily: 'Inter-Bold',
    fontWeight: '600',
    fontSize: 20,
    color: Colors.black,
  },
  semiBold16: {
    fontFamily: 'Inter-SemiBold',
    fontWeight: '500',
    fontSize: 16,
    color: Colors.black,
  },
  semiBold14: {
    fontFamily: 'Inter-SemiBold',
    fontWeight: '500',
    fontSize: 14,
    color: Colors.black,
  },
  semiBold12: {
    fontFamily: 'Inter-SemiBold',
    fontWeight: '500',
    fontSize: 12,
    color: Colors.black,
  },
  // normal
  normal14: {
    fontFamily: 'Inter-Regular',
    fontWeight: '400',
    fontSize: 14,
    color: Colors.black,
  },
  normal12: {
    fontFamily: 'Inter-Regular',
    fontWeight: '400',
    fontSize: 12,
    color: Colors.black,
  },
});

export default Font;
